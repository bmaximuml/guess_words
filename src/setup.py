from setuptools import find_packages, setup

setup(
    name="guess_words",
    version="1.2",
    long_description=__doc__,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    author="Max Levine",
    author_email="max@maxlevine.co.uk",
    url="https://gitlab.com/bmaximuml/guess_words",
)
