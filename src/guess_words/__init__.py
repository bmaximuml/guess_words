from datetime import datetime
from os import environ
from random import shuffle
from secrets import token_hex

import quart.flask_patch
from flask_xcaptcha import XCaptcha
from quart import Quart, flash, redirect, render_template, request
from sqlalchemy.exc import IntegrityError, InvalidRequestError

from guess_words.models import Category, Word, db

from .forms import CategoryForm, MainForm, NewCategoryForm, TeamsForm, WordForm

REQUIRED_VARS = [
    "DB_URI",
]
OPTIONAL_VARS = [
    ("SECRET_KEY", token_hex(64)),
    ("TITLE", "Guess Words"),
    ("AUTHOR", "Max Levine"),
    ("DISABLE_XCAPTCHA", ""),
    ("XCAPTCHA_SITE_KEY", ""),
    ("XCAPTCHA_SECRET_KEY", ""),
    ("XCAPTCHA_VERIFY_URL", ""),
    ("XCAPTCHA_API_URL", ""),
    ("XCAPTCHA_DIV_CLASS", ""),
    ("WORD_BATCH_SIZE", 25),
]


def read_file(file):
    with open(file, "r") as f:
        result = f.read().strip()
        return result


def require_vars(app: Quart, vars: list[str]) -> Quart:
    for var in vars:
        try:
            app.config[var.lower()] = read_file(environ[f"{var}_FILE"])
        except (KeyError, FileNotFoundError):
            try:
                app.config[var.lower()] = environ[var]
            except KeyError:
                raise KeyError(f"{var} environment variable must be set")

    return app

def build_cache(app: Quart, category: str = None):
    print(f"Rebuilding cache for {category or 'all'}", flush=True)
    if category is None:
        cats = Category.query.all()
        category_names = list(map(lambda cat: cat.name, cats))
    else:
        category_names = [category]
    for cat_name in category_names:
        words = Word.query.filter_by(category_name=cat_name).all()
        word_data = list(map(lambda w: w.name, words))
        shuffle(word_data)
        app.config["words"][cat_name] = word_data

def build_app():
    app = Quart(__name__)
    app = require_vars(app, REQUIRED_VARS)

    for var, default in OPTIONAL_VARS:
        try:
            app.config[var.lower()] = read_file(environ[f"{var}_FILE"])
        except (KeyError, FileNotFoundError):
            try:
                app.config[var.lower()] = environ[var]
            except KeyError:
                app.config[var.lower()] = default

    if len(app.config["disable_xcaptcha"]) == 0:
        require_vars(
            app,
            [
                "XCAPTCHA_SITE_KEY",
                "XCAPTCHA_SECRET_KEY",
                "XCAPTCHA_VERIFY_URL",
                "XCAPTCHA_API_URL",
                "XCAPTCHA_DIV_CLASS",
            ],
        )
        app.config["disable_xcaptcha"] = False
    else:
        app.config["disable_xcaptcha"] = True
    print("xCaptcha disabled", flush=True) if app.config["disable_xcaptcha"] else print("xCaptcha enabled", flush=True)

    app.secret_key = app.config.get("secret_key")
    app.config.update(
        {
            "SQLALCHEMY_DATABASE_URI": app.config.get("db_uri"),
            "SQLALCHEMY_TRACK_MODIFICATIONS": False,
            "XCAPTCHA_SITE_KEY": app.config.get("xcaptcha_site_key"),
            "XCAPTCHA_SECRET_KEY": app.config.get("xcaptcha_secret_key"),
            "XCAPTCHA_VERIFY_URL": app.config.get("xcaptcha_verify_url"),
            "XCAPTCHA_API_URL": app.config.get("xcaptcha_api_url"),
            "XCAPTCHA_DIV_CLASS": app.config.get("xcaptcha_div_class"),
        }
    )

    db.init_app(app=app)
    # db.drop_all(app=app)
    # with app.app_context():
    #     db.create_all()
    # db.create_all()
    db.create_all(app=app)
    app.config["words"] = {}

    return app


app = build_app()
app.app_context().push()
if not app.config["disable_xcaptcha"]:
    xcaptcha = XCaptcha(app=app)
else:
    xcaptcha = None

@app.route("/new-category", methods=["GET", "POST"])
async def new_category():
    form = NewCategoryForm()
    if request.method == "POST":
        if form.validate() and (xcaptcha is None or xcaptcha.verify()):
            db.session.add(Category(name=form.category.data))
            app.config["words"][form.category.data] = []
            try:
                db.session.commit()
                await flash("Category successfully created!")
            except IntegrityError:
                await flash("Category exists, skipping.")
                db.session.rollback()
        elif not form.validate():
            await flash("Invalid data supplied, category not created.")
        elif xcaptcha is not None and not xcaptcha.verify():
            await flash("Bot suspectde, category not created.")
        else:
            await flash("Unknown error occurred, category not created.")
        # return redirect('/')

    return await render_template(
        "new-category.html",
        form=form,
        year=datetime.now().year,
        title=app.config.get("title"),
        author=app.config.get("author"),
    )


@app.route("/all", methods=["GET"])
async def all():
    word_data = Word.query.order_by(Word.category_name).order_by(Word.name).all()
    return await render_template(
        "all.html",
        word_data=word_data,
        total_count=len(list(word_data)),
        year=datetime.now().year,
        title=app.config.get("title"),
        author=app.config.get("author"),
    )


@app.route("/play", methods=["GET", "POST"])
async def play():
    all_category = "All ♠"
    cats = Category.query.order_by(Category.name).all()
    category_names = list(map(lambda cat: cat.name, cats))

    form = CategoryForm()
    form.category.choices = category_names + [all_category]

    if request.method == "POST":
        if form.validate():
            if form.category.data != all_category:
                category_names = [form.category.data]
                word_batch_size = int(app.config.get("word_batch_size"))
            else:
                word_batch_size = int(int(app.config.get("word_batch_size")) / len(category_names)) + 1
            word_data = []
            for category_name in category_names:
                try:
                    cat_word_data = app.config["words"][category_name][:word_batch_size]
                except KeyError:
                    build_cache(app, category_name)
                    cat_word_data = app.config["words"][category_name][:word_batch_size]
                del app.config["words"][category_name][:word_batch_size]
                if len(cat_word_data) < word_batch_size:
                    build_cache(app, category_name)
                    total_in_cat = Word.query.filter_by(category_name=category_name).count()
                    if len(cat_word_data) == 0 or total_in_cat > (2 * word_batch_size):
                        cat_word_data = app.config["words"][category_name][:word_batch_size]
                        del app.config["words"][category_name][:word_batch_size]
                word_data += cat_word_data
            shuffle(word_data)

            return await render_template(
                "play.html",
                word_data=word_data,
                category_names=category_names,
                form=form,
                year=datetime.now().year,
                title=app.config.get("title"),
                author=app.config.get("author"),
            )
        else:
            await flash("Something broke.")
            return redirect("/")

    return await render_template(
        "play.html",
        category_names=category_names,
        form=form,
        year=datetime.now().year,
        title=app.config.get("title"),
        author=app.config.get("author"),
    )


@app.route("/teams", methods=["GET", "POST"])
async def teams():
    form = TeamsForm()
    teams = None
    if request.method == "POST":
        if form.validate():
            quantity = int(form.quantity.data)
            names = [name for name in form.names.data.splitlines() if name]
            shuffle(names)
            teams = [[] for _ in range(quantity)]
            for i, name in enumerate(names):
                team_num = i % quantity
                try:
                    teams[team_num].append(name)
                except IndexError:
                    teams[team_num] = [name]
        else:
            await flash("Unknown error occurred, please try again.")

    return await render_template(
        "teams.html",
        teams=teams,
        form=form,
        year=datetime.now().year,
        title=app.config.get("title"),
        author=app.config.get("author"),
    )


@app.route("/", methods=["GET", "POST"])
async def dynamic():
    category_data = Category.query.order_by(Category.name).all()
    category_names = list(map(lambda cat: cat.name, category_data))

    form = MainForm()
    form.category.choices = category_names
    template_form = WordForm(prefix="words-_-")

    if request.method == "POST":
        if form.validate() and (xcaptcha is None or xcaptcha.verify()):
            for word in form.words.data:
                word = word["word"].strip().title()
                if len(word) > 0:
                    print(word, flush=True)
                    while True:
                        db.session.add(Word(name=word, category_name=form.category.data))
                        try:
                            db.session.commit()
                            break
                        except IntegrityError:
                            break
                        except InvalidRequestError:
                            db.session.rollback()
            await flash("Words successfully inserted!")
        elif not form.validate():
            await flash("Invalid data supplied, words not submitted.")
        elif xcaptcha is not None and not xcaptcha.verify():
            await flash("Bot suspected, words not submitted.")
        else:
            await flash("Unknown error occurred, words not submitted.")
        return redirect("/")

    word_data = Word.query.order_by(Word.category_name).all()
    return await render_template(
        "dynamic.html",
        form=form,
        category_data=category_data,
        word_data=word_data,
        total_count=len(list(word_data)),
        year=datetime.now().year,
        title=app.config.get("title"),
        author=app.config.get("author"),
        _template=template_form,
    )


@app.route("/status", methods=["GET", "POST"])
async def status():
    return "Active\n"


if __name__ == "__main__":
    app.debug = True
    app.run()
