const ID_RE = /(-)_(-)/;

/**
 * Replace the template index of an element (-_-) with the
 * given index.
 */
function replaceTemplateIndex(value, index) {
    return value.replace(ID_RE, '$1'+index+'$2');
}


/**
 * Add a new form.
 */
function addForm() {
    var $templateForm = $('#word-_-form');

    if ($templateForm.length === 0) {
        console.log('[ERROR] Cannot find template');
        return;
    }

    // Get Last index
    var $lastForm = $('.subform').last();

    var newIndex = 0;

    if ($lastForm.length > 0) {
        newIndex = parseInt($lastForm.data('index')) + 1;
    }

    // Maximum of 200 subforms
    if (newIndex >= 200) {
        console.log('[WARNING] Reached maximum number of elements');
        return;
    }

    // Add elements
    var $newForm = $templateForm.clone();

    $newForm.attr('id', replaceTemplateIndex($newForm.attr('id'), newIndex));
    $newForm.data('index', newIndex);

    $newForm.find('label, input, select, textarea').each(function(idx) {
        var $item = $(this);

        if ($item.is('label')) {
            // Update labels
            $item.attr('for', replaceTemplateIndex($item.attr('for'), newIndex));
            return;
        }

        // Update other fields
        $item.attr('id', replaceTemplateIndex($item.attr('id'), newIndex));
        $item.attr('name', replaceTemplateIndex($item.attr('name'), newIndex));
    });

    // Append
    $('#subforms-container').append($newForm);
    $newForm.addClass('subform');
    $newForm.removeClass('is-hidden');
}

function nextWord() {
    let current = document.querySelector('.play-words > .notification.is-active');
    console.log('current');
    console.log(current);
    let nextSibling = current.nextElementSibling;
    console.log('nextSibling');
    console.log(nextSibling);

    if (!nextSibling || !nextSibling.classList.contains('play-word')) {
        let play_form = document.getElementById("play-form");
        console.log('play_form');
        console.log(play_form);
        play_form.submit();
    } else {
        // Add word to past words list
        const pastWord = document.querySelector('.play-words > .notification.is-active > h3.title');
        const pastWords = document.querySelector('.past-words');
        const node = document.createElement("li");
        const textnode = document.createTextNode(pastWord.innerHTML);
        node.appendChild(textnode);
        pastWords.appendChild(node);

        // Display next word
        current.classList.remove('is-active');
        current.classList.add('is-hidden');
        nextSibling.classList.remove('is-hidden');
        nextSibling.classList.add('is-active');
    }
}

/* Start Menu controls */

document.addEventListener('DOMContentLoaded', () => {
    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                if ($target.classList.contains('slidein')) {
                    slideMenuOut();
                }
                else {
                    slideMenuIn();
                }
            });
        });
    }

    const navbar_items = Array.prototype.slice.call(document.querySelectorAll('.navbar-item'), 0);
    navbar_items.forEach(item => {
        item.addEventListener('click', () => {
            slideMenuOut();
        })
    })
});

function slideMenuIn() {
    const $main_navbar = document.getElementById('main_navbar');
    $main_navbar.classList.add('is-active');
    $main_navbar.classList.remove('slideout');
    $main_navbar.classList.add('slidein');
    toggleBurgers();
}

function slideMenuOut() {
    const $main_navbar = document.getElementById('main_navbar');
    $main_navbar.classList.remove('slidein');
    $main_navbar.classList.add('slideout');
    $main_navbar.addEventListener('animationend', function() {
        if ($main_navbar.classList.contains('slideout')) {
            $main_navbar.classList.remove('is-active');
        }
    });
    toggleBurgers();
}

function toggleBurgers() {
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(el => {
            // Toggle the "is-active" class on the "navbar-burger"
            el.classList.toggle('is-active');
        });
    }
}

/* End Menu controls */

function increment() {
    const $newCount = Number(document.querySelector('.count').innerHTML) + 1;
    const $countElementAll = Array.prototype.slice.call(document.querySelectorAll('.count'), 0);

    $countElementAll.forEach(el => {
        el.innerHTML = $newCount;
    });
}

function resetCount() {
    document.querySelectorAll('.count').forEach(el => {
        el.innerHTML = 0;
    });
    document.querySelectorAll('.past-words').forEach(el => {
        el.replaceChildren();
    });
}

$(document).ready(function() {
    // Bind clicks
    $('#add').click(addForm);
    $('.next-word').click(nextWord);
    $('.increment').click(increment);
    $('.reset-count').click(resetCount);

    // Add forms
    box_count = document.getElementById('subforms-container').childElementCount;
    min_boxes = 7;
    extra_boxes = box_count < min_boxes ? min_boxes - box_count : 0;
    for (let i = 0; i < extra_boxes; i++) {
        addForm();
    }
});
