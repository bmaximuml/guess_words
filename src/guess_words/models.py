from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Category(db.Model):
    name = db.Column(db.String(200), primary_key=True, nullable=False)
    words = db.relationship("Word", backref="category", lazy=True)


class Word(db.Model):
    name = db.Column(db.String(200), primary_key=True, nullable=False, unique=True)
    category_name = db.Column(db.String(200), db.ForeignKey("category.name"), primary_key=True, nullable=False)
