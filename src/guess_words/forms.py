from flask_wtf import FlaskForm
from wtforms import FieldList, Form, FormField, IntegerField, SelectField, StringField, SubmitField, TextAreaField
from wtforms.validators import InputRequired, NumberRange


class SubmitForm(FlaskForm):
    submit_f = SubmitField("Submit", render_kw={"class": "button is-link"})


class CategoryForm(SubmitForm):
    category = SelectField("Category", validators=[InputRequired()], render_kw={"class": "select"})


class NewCategoryForm(SubmitForm):
    category = StringField("Category", validators=[InputRequired()], render_kw={"class": "input"})


class WordForm(Form):
    """Subform.

    CSRF is disabled for this subform (using `Form` as parent class) because
    it is never used by itself.
    """

    word = StringField("Word", render_kw={"placeholder": "Enter your word...", "class": "input"})


class MainForm(CategoryForm):
    """Parent form."""

    words = FieldList(FormField(WordForm), min_entries=1, max_entries=200)


class TeamsForm(SubmitForm):
    """Teams generator form"""

    quantity = IntegerField(
        "Quantity", validators=[InputRequired(), NumberRange(min=1)], render_kw={"class": "input", "type": "number"}
    )
    names = TextAreaField(
        "Names",
        validators=[InputRequired()],
        render_kw={"class": "textarea", "placeholder": "List\nnames\nwith\none\nname\nper\nline", "rows": "10"},
    )
