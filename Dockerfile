# syntax=docker/dockerfile:1
FROM python:3.12-slim-bookworm

ARG USER=guess_words
ARG WORKDIR=/run/$USER

RUN set -aeux \
    ; apt-get update \
	; apt-get install -y --no-install-recommends \
		build-essential \
        curl \
        default-libmysqlclient-dev \
        pkg-config \
        python3-dev \
        gcc \
	; rm -rf /var/lib/apt/lists/* \
    ; useradd \
        -M \
        ${USER}

WORKDIR ${WORKDIR}

COPY --chown=${USER}:${USER} src/requirements.txt .

RUN pip install -r requirements.txt

COPY --chown=${USER}:${USER} src .

EXPOSE 80

USER ${USER}

CMD hypercorn -w 1 --debug --log-level debug -b 0.0.0.0:80 guess_words:app

HEALTHCHECK --interval=10s CMD curl --fail localhost:80/status || exit 1
