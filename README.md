# Guess Words

Create fun word games.

Supply words and categories which will then be served to you randomly.

## Configuration

All configuration options are set using environment variables.

All variables can also be set from a file, by setting an environment variable with the suffix `_FILE` to the path to the file containing the variable value.
_FILE variables will take precedence over non-_FILE variables.
This can be used in conjunction with docker secrets.

### Database Configuration

Set the `DB_URI` environment variable to a mariadb / mysql connection URI, including the table name.
E.g. `mysql+pymysql://guess_user:guess_password@guess_db/guess_table`

### xCaptcha Configuration

An xCaptcha implementation is ~mandatory~ strongly recommended to protect this site.
[Flask-xCaptcha](https://github.com/bmaximuml/flask-xcaptcha) is used to setup the xCaptcha implemention.

You must set the following environment variables to their appropriate values. See [Flask-xCaptcha](https://github.com/bmaximuml/flask-xcaptcha) for a detailed description of each of these variables.

- `XCAPTCHA_SITE_KEY`: Site key provided by xCaptcha service
- `XCAPTCHA_SECRET_KEY`: Secret key provided by xCaptcha service
- `XCAPTCHA_VERIFY_URL`: The URL to verify the filled in xCaptcha at
- `XCAPTCHA_API_URL`: The URL of the xCaptcha API JS script
- `XCAPTCHA_DIV_CLASS`: The class of the div element surrounding the xCaptcha

Alternatively, you may disable xCaptcha by setting the `DISABLE_XCAPTCHA` variable to any string.

#### hCaptcha

We recommend using [hCaptcha](https://www.hcaptcha.com/). hCaptcha provides a free, privacy respecting xCaptcha implementation.

Most hCaptcha implementations will use the following values:
 
 - `XCAPTCHA_VERIFY_URL`: `https://hcaptcha.com/siteverify`
 - `XCAPTCHA_API_URL`: `https://hcaptcha.com/1/api.js`
 - `XCAPTCHA_DIV_CLASS`: `h-captcha`

 You will need to create a site from the [hCaptcha dashboard](https://dashboard.hcaptcha.com/overview) in order to generate a unique site key and secret key.

#### Google reCAPTCHA

You can also use [Google reCAPTCHCA](https://developers.google.com/recaptcha/).


Most Google reCAPTCHCA implementations will use the following values:
 
 - `XCAPTCHA_VERIFY_URL`: `https://www.google.com/recaptcha/api/siteverify`
 - `XCAPTCHA_API_URL`: `//www.google.com/recaptcha/api.js`
 - `XCAPTCHA_DIV_CLASS`: `g-recaptcha`

### Optional Configuration

The following environment variables are also available.

| Variable | Description                                                       | Default       |
| ---          |-------------------------------------------------------------------|---------------|
| SECRET_KEY | Cryptographic key to be used where appropriate (currently unused) | token_hex(64) |
| TITLE | Website title                                                     | Guess Words   |
| AUTHOR | Site author, used in copyright text                               | Max Levine    |
| WORD_BATCH_SIZE | Number of words to grab at a time when playing                    | 25            |
